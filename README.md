# k3s_projects-repo

This repo will capture the various experiments with K3s.
- https://k3s.io


## Gitlab Runner

Great article here:
https://adambcomer.com/blog/setup-gitlab-cicd-on-kubernetes.html

## DNS Issue On CentOS

```bash
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```
- https://github.com/kubernetes/kubernetes/issues/21613#issuecomment-363859075
